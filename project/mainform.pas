unit MainForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, EditBtn, StdCtrls,
  ComCtrls, Encryption, FileUtil, DCPrijndael;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    DCP_rijndael1: TDCP_rijndael;
    Edit1: TEdit;
    FileNameEdit1: TFileNameEdit;
    Label1: TLabel;
    Label2: TLabel;
    SaveDialog1: TSaveDialog;
    TabControl1: TTabControl;
    procedure Button1Click(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.TabControl1Change(Sender: TObject);
begin
  if TabControl1.TabIndex = 0 then
  begin
    Button1.Caption := 'Encrypt';
    FileNameEdit1.Filter:='All Files | *';
  end
  else
  begin
    Button1.Caption := 'Decrypt';  
    FileNameEdit1.Filter:='Encrypted Files (*.crypt) | *.crypt';
  end;
  FileNameEdit1.Clear;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  inFile, outFile: TFileStream;
begin    
  if TabControl1.TabIndex = 0 then
  begin
    SaveDialog1.Filter:= 'Encrypted Files (*.crypt) | *.crypt';
    SaveDialog1.FileName:=FileNameEdit1.FileName+'.crypt';
  end
  else
  begin
    SaveDialog1.Filter:= 'All Files | *';
    SaveDialog1.FileName:=ChangeFileExt(FileNameEdit1.FileName, '');
  end;
  if not SaveDialog1.Execute then
    Exit;
  inFile := TFileStream.Create(FileNameEdit1.FileName, fmOpenRead);
  try
    outFile := TFileStream.Create(SaveDialog1.FileName, fmCreate);
    try
      if TabControl1.TabIndex = 0 then
        EncryptData(inFile, outFile, Edit1.Text, DCP_rijndael1)
      else
        DecryptData(inFile, outFile, Edit1.Text, DCP_rijndael1);
    finally
      outFile.Free;
    end;
  finally
    inFile.Free;
  end;
end;

end.

