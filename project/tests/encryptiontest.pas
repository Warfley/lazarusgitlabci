unit EncryptionTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, DCPrijndael, Encryption;

type

  TEncDecTest= class(TTestCase)
  private
    FCipher: TDCP_rijndael;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHookUp;
  end;

implementation

procedure TEncDecTest.TestHookUp;
var
  origMessage, pass, encDecMessage: String;
  encStream, clearStream: TStringStream;
begin
  origMessage := 'Hello World!';
  pass := 'Password';
  clearStream := TStringStream.Create(origMessage);
  encStream := TStringStream.Create('');
  try
    EncryptData(clearStream, encStream, pass, FCipher);
    // reset streams
    clearStream.Position := 0;  
    encStream.Position := 0;
    // Delete previous data
    clearStream.Size := 0;
    DecryptData(encStream, clearStream, pass, FCipher);
    encDecMessage := clearStream.DataString;
  finally
    encStream.Free;
    clearStream.Free;
  end;
  AssertEquals('Decryption of Encryption is expected to be equal', origMessage, encDecMessage);
end;

procedure TEncDecTest.SetUp;
begin
  FCipher := TDCP_rijndael.Create(nil);
end;

procedure TEncDecTest.TearDown;
begin
  FCipher.Free;
end;

initialization

  RegisterTest(TEncDecTest);
end.

