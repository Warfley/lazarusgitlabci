unit Encryption;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DCPblockciphers, DCPsha256;

procedure EncryptData(inData: TStream; outData: TStream; const pass: String; cipher: TDCP_blockcipher128);
procedure DecryptData(inData: TStream; outData: TStream; const pass: String; cipher: TDCP_blockcipher128);
implementation

procedure EncryptData(inData: TStream; outData: TStream; const pass: String;
  cipher: TDCP_blockcipher128);
begin
  cipher.InitStr(pass, TDCP_sha256);
  cipher.EncryptStream(inData, outData, inData.Size);
end;

procedure DecryptData(inData: TStream; outData: TStream; const pass: String;
  cipher: TDCP_blockcipher128);
begin
  cipher.InitStr(pass, TDCP_sha256);
  cipher.DecryptStream(inData, outData, inData.Size);
end;

end.

