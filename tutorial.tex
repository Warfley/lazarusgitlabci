\documentclass{article}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{listings}

\title{Automated building for Lazarus Projects using GitLabs CI/CD}
\author{Frederic Kehrein}
\date{\today}

\begin{document}
\maketitle
\newpage
\section{Introduction}
This work aims to get a broad overview about using GitLabs CI/CD system together
with Lazarus projects.\\
While we will take a look at GitLabs CI and how to use it on Lazarus projects,
this is not intended to be a comprehensive tutorial on Git, GitLab or even
Lazarus for that matter. For further informations, please checkout dedicated
resources like the GitLab documentation.\\
After this tutorial you should know how to basically setup a project for
automated deployment for Linux and Windows using different widgetsets, add a
CI/CD configuration for GitLab and let it automatically build, test and deploy
your project.
\subsection{CI/CD}
\textbf{CI/CD} stands for \textit{Continuous Integration} and
\textit{Continuous Delivery/Deployment}. These are steps to automatically build, test
and distribute software using version control platforms like GitLab.\\
At it's core these are automated steps to be taken whenever a certain event
occured. For example whenever a new commit is pushed to the git, you might want
to compile the project and run the unit tests to ensure the new functionality
does not break anything. Or you might want to package up a release whenever a
new tag is added and automatically upload that package for others to download.
Another common usecase is, when working on a service, e.g. a website, you might
want to deploy the service directly, so the backend is updated whenever you
package a new release.\\
\\
GitLab is a software for managing Git repositories, which includes many features
for software development and project management.
\href{https://gitlab.com}{gitlab.com} is an open service running GitLab, but as
the software is OpenSource, you can also run your own GitLab service on your
own, e.g. company internal, Server.\\
One of the features GitLab is most well known for is its CI/CD toolchain, which
is the focus of this work. If you are interested in more information take a
look at the \href{https://docs.gitlab.com/ee/ci/introduction/}{GitLab
  Documentation}.\\
\\
For completeness sake I should mention that there are many other plattforms
with similar features, such as
\href{https://github.com}{GitHub} and \href{https://bitbucket.org/}{Bitbucket}
are comparable to gitlab.com.\\
There are also OpenSource software alternatives like
\href{https://gitea.io/en-us/}{GitTea} or \href{https://gogs.io/}{Gogs}, but
these don't have any inbuilt CI and are therefore not of relevance for us.\\
The reason this is about GitLab and not the more popular platform GitHub, or
any other platform, is simply that I have more experience with GitLab and also
have a private GitLab instance running which I use for most of my projects. But
many of the techniques represented in this work will surely also work on other
platforms.
\subsection{Lazarus}
By the fact you are currently reading this paper, I think it is safe to assume
that you have at least some knowledge about Lazarus. Lazarus is a FreePascal IDE
which manages files, packages, build options, etc. in form of projects. To build
a Lazarus project you need Lazarus. And here lies the crux of this work, Lazarus
is a graphical IDE build to be used by a user. Usually the developer has his
local Lazarus+FPC installation, readily configured for his exact needs,
installed packages, cross compilers, etc. But only on his machine. For automated
building this needs to be portable and completely executable by a machine
without any user interaction.\\
\\
This disconnect between the Lazarus IDE, built for users and the automated
nature of CI/CD is in my oppinion the main reason we don't see much automation
for the development cycle of Lazarus projects.\\
This work will introduce tools and a workflow, which makes it rather easy to
automate your development life-cycle.

\section{The Project}
The project can be found in the very same repository of this tutorial on
\href{https://gitlab.com/Warfley/lazarusgitlabci}{gitlab.com}. It is a simple
GUI tool for encrypting and decrypting files using AES encryption.\\
The encryption is done using the
\href{https://wiki.lazarus.freepascal.org/DCPcrypt}{DCPcrypt} library which I
installed on my development Lazarus installation via the OPM and is built with
Lazarus 2.0.6. For our CI we want to have the same conditions, as using
different versions might result in bugs when building, I can't reproduce on my
development version.\\
This project also includes a unit-test using a FPCUnit Console Test Application
created with Lazarus. Our goal is to create a CI which tries to build the
project, runs the unit tests, and if it is tagged, uploads the build artifacts
for releases.\\
The release should be for x86\_64 Linux and Windows, with both GTK2 and QT5
widgetset on Linux and Windows-Forms widgetset on Windows.
\subsection{Setting up Build Modes}
For doing so we first need to prepare our Project. Crucial for this task is the
use of \textit{Build Modes}. These are targets that Lazarus provides to
enable building with different configurations for different circumstances.\\
For our release we need 3 distinct release build modes \textit{Windows},
\textit{GTKLinux} and \textit{QTLinux}. To configure build modes simply go to
\textit{Project Options $\rightarrow$ Compiler Options}. In the window there should now be
on the top a label \textit{Build Modes} and a combobox that says ``Default'',
which is your currently selected build mode. Figure \ref{fig:CompilerOptions}
shows this. Simply click on the button labled ``...'' to enter the build mode
editor shown on the left in figure \ref{fig:BuildModes}.\\
\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{Images/CompilerOptions.png}
  \caption{Compiler Options}
  \label{fig:CompilerOptions}
\end{figure}
The window that is now popping up can be used to add and delete build modes
using the + and the - key. As this is our first creation of build modes, we make
use of the button on the top \textit{Create Debug and Release modes}, which will
create two new modes \textit{Debug} and \textit{Release} with usefull
configurations for each. We then dublicate the Release mode twice, by first
selecting the ``Release'' entry in the list and then clicking the plus button
twice.\\
These modes we now name \textit{GTKLinux}, \textit{QTLinux} and
\textit{Windows}. The result should look like the right image in figure \ref{fig:BuildModes}.\\
\begin{figure}
  \includegraphics[width=0.5\linewidth]{Images/BuildModes1.png}
  \includegraphics[width=0.5\linewidth]{Images/BuildModes2.png}
  \caption{Build Modes}
  \label{fig:BuildModes}
\end{figure}
Now we need to configure these modes such that they build our different targets.
We first need to ensure that they don't override each others compilation result,
so we begin with changing the \textit{Target file name (-o)} option under
\textit{Project Options $\rightarrow$ Compiler Options $\rightarrow$ Paths}. First we need to select
the respected build mode in the drop down menu on the top and then change the
value. I choose as a build path
\textit{build/Release/\$(TargetCPU)-\$(TargetOS)/FileEncrypterGTK}. This can be
seen on the left in figure \ref{fig:Configuration}. The two
macros \textit{TargetCPU} and \textit{TargetOS} will be filled out by Lazarus on
compilation. The only thing that needs to be added by hand is the GTK at the end
to indicate that this is the GTK build. We now repeat this for the QTLinux and
Windows build mode. Of course we need to adjust the name to FileEncrypterQT and
FileEncrypter and make sure that the \textit{Apply conventions} checkbox is
checked, so Lazarus will automatically add \textit{.exe} for Windows.\\
\begin{figure}
  \includegraphics[width=0.5\linewidth]{Images/Paths.png}
  \includegraphics[width=0.5\linewidth]{Images/Targets.png}
  \caption{Mode Configuration}
  \label{fig:Configuration}
\end{figure}
\\
Next we need to configure the targets. This can be done at \textit{Project
  Options $\rightarrow$ Compiler Options $\rightarrow$ Config and Targets}. For all three build modes
we choose \textit{x86\_64} as \textit{Target CPU family (-P)} and as
\textit{Target OS (-T)} we choose \textit{Linux} for both Linux builds and
\textit{Win64} for the Windows build. This can be seen in figure
\ref{fig:Configuration} on the right.\\
Lastly we need to configure the widgetset. This can be done in \textit{Project
  Options $\rightarrow$ Compiler Options $\rightarrow$ Additions and Overrides}. Here is a button
\textit{Set ``LCLWidgetSetType''}. We press this three times to add the three
widget sets \textit{qt5}, \textit{gtk2} and \textit{win32}. Via the checkboxes
in the \textit{Targets} area you can select which widgetset should be used by
which build mode. The result should look something like \ref{fig:widgetsets}.\\
\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{Images/Widgetset.png}
  \caption{Widgetset Configuration}
  \label{fig:widgetsets}
\end{figure}
\\
After this is all done, you can simply save and close the project settings by
clicking \textit{Ok}. You might get an Error message like in \ref{fig:error},
because you are missing any cross compilers locally, but this is fully ok,
because these modes are made for compiling on the CI not your local
installation.\\
\begin{figure}
  \centering
  \includegraphics[width=0.4\linewidth]{Images/error.png}
  \caption{An expected Error}
  \label{fig:error}
\end{figure}
This is now all that needed to be done in Lazarus. To recap, we first added new
build modes for our release builds. We used the default \textit{Release} mode as
template for this. We then configured the Paths, target CPU and OS as well as
the widget set for each build mode.

\section{GitLab CI}
First we need to understand how GitLab handles the CI. By placing a
\textit{.gitlab-ci.yml} file into the root of your repository, GitLab knows
that you want to use the CI and uses this YAML script as instructions what to
do.\\
The CI script is then executed by a Runner. These are processes that connect to
GitLab and check if there is a job available. If so, they download the
repostiory and start executing the script.\\
Runners can either be self hosted, e.g. on your own Server or PC, or provided.
gitlab.com provides free runners for public OpenSource projects and paid plans
for closed source projects. When running your own GitLab instance, you need to
register your own runners. Describing how to register runners is out of the
scope for this tutorial. See the \href{https://docs.gitlab.com/runner/}{GitLab
  Documentation} for further information. For this tutorial we will be using the
free runners provided by GitLab for public projects\\
\\
There are multiple different kinds of runners, the most simple one is the native
runner. This runner executes the CI script basically as a shell script on it's
local machine. While this is of course really simple, especially if you provide
the server the runner runs on, you can simply configure it such that it provides
all the tools and resources required for building. But this is rather
unflexible. Especially if you share this runner you might not want everyone to
install his exact dependencies on that one machine.\\
\subsection{Docker}
Another option is using a \textit{Docker} runner. Docker uses containers, which use the Kernel of
the host machine, but provide a complete environment, that is shut down to the
outside. This way we can provide exactly the tools we need via a Docker image,
without having to worry if the target runner provides anything.\\
\\
Docker images come in many flavours, for example you can simply use a Debian
docker image and install everything for yourself. For our project this would be
installing Lazarus and FPC via apt-get and then downloading our dependencies.
But we don't want to take a lot of our CI time for simply installing our
dependencies again and again, especially if we pay for that time. So an alternative is to use prebuild images.\\
\\
I've build some Docker images that provide Lazarus+FPC installed via fpcup as
well as LPM, a tool I've developed for managing dependencies, which we will also
use later on. These images can be found at the
\href{https://hub.docker.com/repository/docker/warfley/lpm}{Docker Hub} and for
the rest of this tutorial we don't need to worry about anything docker related
anymore.
\subsection{.gitlab-ci.yml}
Now let's get to the meat of the matter and create our ci script. For this we
first open the \textit{.gitlab-ci.yml} file in the root of our repository with
any text editor of our choice. I recommend an editor that supports YAML like vim
or emacs in YAML-mode.\\
\\
A CI has \textit{stages} and each stage has multiple \textit{jobs}. Every stage
can only start if the one before succeded, but inside a stage every job can run
in parallel. In our project we want to build, test and then
deploy. We can't test before we are done building, and we can't deploy if
our tests are failing (as we don't want to deploy broken software, we are not
Microsoft). So we have three stages \textit{build}, \textit{test} and
\textit{publish}. While we have only one test, we don't need multiple jobs per
stage, but if we had more than one test, we could run them in parallel by
creating a job for each.\\
But one thing after the other, we first need to define our stages in YAML:\\
\begin{lstlisting}
stages:
  - build
  - test
  - publish
\end{lstlisting}~\\
\\
We now define our build job for the build stage. It should build both, the final
project, as well as the unittest project:\\
\begin{lstlisting}
build:
  stage: build
  image: warfley/lpm:2.0
  artifacts:
    expire_in: 1 hour
    paths:
      - project/build
      - project/tests/UnitTests
  script:
    - lpm self-update && lpm update
    - lpm build --yes project/FileEncrypter.lpi QTLinux GTKLinux Windows
    - lpm build --yes project/tests/UnitTests.lpi
\end{lstlisting}~\\
This job has the name \textit{build} and is also part of the stage
\textit{build}. It uses the docker image \textit{warfley/lpm} with tag
\textit{2.0} i.e. the one that provides Lazarus 2.0(.6), which is the version we
built our project on. The \textit{artifacts} section of this job defines the
build artifacts that are required for the next stages. These are the unittests,
as well as the build results for publishing. A timeout of 1 hour should be
enough to not waste memory on temporary build results.\\
The job then executes each line in the script section inside a shell.
\paragraph{lpm}
To build this we are using the
\href{https://github.com/Warfley/LazarusPackageManager}{Lazarus Package Manager
  LPM} a script, I've written for exactly this purpose. We could use the
\textit{lazbuild} executable included in the Lazarus installation directly, but
this does not handle package dependencies. As you might recall we are using
DCPcrypt from the OPM. LPM allows us to search the OPM and install packages from
there. It can also be used to install packages from other sources like HTTP
downloads, git or svn repositories.\\
We first need to call lpm self-update to use git to check for new versions of
the lpm script. This ensures we are always using the most current version of
lpm. Then lpm update is called, to download the package list for OPM packages,
so we know which packages are in the OPM.\\
We could now use the lpm to install DCPcrypt, but rather we use \lstinline{lpm build --yes}, which automatically checks if required packages are missing from
the OPM and installs them before compiling the program using lazbuild.\\
We use lpm build to build the modes QTLinux GTKLinux and Windows for the
FileEncrypter project and build the only build mode (Default) for the UnitTests
project.\\
\\
The next stage is \textit{test}. We also add a job named \textit{test} for this:\\
\begin{lstlisting}
test:
  stage: test
  dependencies:
    - build
  script:
    - project/tests/UnitTests --all | grep "<NumberOfFailures>0</NumberOfFailures>"
\end{lstlisting}~\\
Here we don't require any special image, but only the results from
\textit{build}. We then execute the unittest and check if the number of failed
tests is equal to 0. Here could be any kind of shell script testing the unit
test, the grep is just the most simple option.\\
\\
Lastly we add our publishing job, which uploads our builded software.\\
\begin{lstlisting}
publish:
  stage: publish
  only:
    - tags
  dependencies:
    - build
  artifacts:
    paths:
      - Release
  script:
    - cp -R project/build/Release Release
\end{lstlisting}~\\
In this case we use GitLabs CI artifacts as publishing mechanism. While this is
defenetly not a great solution, it's quick and easy and does it's job. We also
only want to do this when we add a tag, i.e. developed a new version and tagged
it in git. We don't need to publish every single commit.\\
Of course this script could do much more, for example packaging the build
artifacts with required config files or so, but for our purpose we only need the
Release builds.\\
\\
The full script can be found in the GitLab repository:
\href{https://gitlab.com/Warfley/lazarusgitlabci/-/blob/master/.gitlab-ci.yml}{Link}
\subsection{Pushing and Waiting}
After all this we now need to simply add our changes to the git and push them as
a commit:\\
\begin{lstlisting}[language=bash]
> git add .gitlab-ci.yml
> git commit -a -m "Adding CI"
> git push
\end{lstlisting}~\\
\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{Images/CI.png}
  \caption{CI Results}
  \label{fig:ci}
\end{figure}\\
And now we wait. When going to the projects GitLab page, we have a tab called
\textit{CI/CD} on the left menu. Clicking on it reveals the state of the
piplines for this project. The status of this run can be seen in figure
\ref{fig:ci}. If you struggle creating the ci script, don't worry, as you can
see it also took me multiple attempts to create that CI script you see above.\\
\\
You can also get more information by clicking on the pipeline number or the
jobs. You can take a look at the publicly available pipeline page of this
repository and play around with it:
\href{https://gitlab.com/Warfley/lazarusgitlabci/pipelines}{Link}.\\
\\
Up to now we only used the CI to build and test, the last step is now to test
the deployment by tagging this to create a release:\\
\begin{lstlisting}[language=bash]
> git tag -a -m "Version 1.0" v1  
> git push origin v1
\end{lstlisting}~\\
And again we now wait and can inspect the progress when visiting the projects CI
site on GitLab.\\
When this is done and we look into the \textit{publish} job, we have in the menu
on the right an entry called artifacts with three buttons: \textit{Keep}
\textit{Browse} and \textit{Download}. If the time limit of the artifacts is
forever, then the Keep button is not visible, but only Browse and Download.\\
These buttons can now be used to get a download link for the release of your
software. If you want to test out my glorious file encrypter simply visit my
GitLab page and download the version for your operating system:
\href{https://gitlab.com/Warfley/lazarusgitlabci/-/jobs/508059548/artifacts/browse}{Link}.

\section{Final Words}
This was a very brief outlook into the world of CI/CD with GitLab and Lazarus.
There is much more you can do with the GitLab CI scripts, which is completely
out of scope for this tutorial. But with what we done to this point, you should
be able to automate most of your projects. But there are still some things I
want to mention.\\
\\
\paragraph{Extending the Images}
The Docker lpm images are rather basic and only provide what is nessecary for
comping for x86\_64 linux and x86\_64 and i386 Windows. If you need more you can
first of all use the software provided inside the images in your ci script. E.g.
you need sqlite for running your tests? Simply call \lstinline{apt-get update \&\& apt-get install libsqlite3-dev} in your ci script.\\
If you need more cross compilers, I would recommend you extending the docker
Images by creating your own docker file on top of one of the lpm images, and
using the installed \lstinline{fpcup} binary to install more cross compilers.
Further information can be found at the Docker-Hub page:
\href{https://hub.docker.com/repository/docker/warfley/lpm}{Link}.\\
If you build new cross compilers images you think are usefull for others as
well, feel free to check out my GitHub repository
\href{https://github.com/Warfley/LPMDocker}{Link} on how to build such an LPM
image. You could simply add one and make a pull request, so I can add it to the
Docker Hub, so everyone can use it as the others.\\
Some usefull cross compilers would be ARM and i386 Linux as well as Android. But
I honestly don't know much about cross compiling so I haven't done it already.\\
\\
\paragraph{Bug Reports}
If you encounter bugs in LPM or the Docker images feel free to open up an issue
on the LPM GitHub \href{https://github.com/Warfley/LazarusPackageManager}{Link}
or the LPMDocker GitHub \href{https://github.com/Warfley/LPMDocker}{Link}
respectively.\\
\\
I'll try my best to fix any bugs and also to keep the Docker images up to date.
If you are missing some Lazarus version or so, also leave a ticket. Currently
the Docker-Hub contains images for 1.6, 1.8, 2.0, (lazarus-)trunk and experimental which is
Lazarus trunk + FPC trunk. Simply check them out by using
\lstinline{warfley/lpm:trunk} or so as image.\\
\\
With that said, I hope, even though this barely scratches the surface of what is
possible with CI/CD, that I teasered you enough to give it a shot.\\
\\
Happy hacking!
\end{document}